### Usage

#### For the reveal animation
* Define a colour resource called `bbcOrange`
* Copy the contents of dimens.xml to your dimension resources
* Copy the ic_bbc_logo.xml and brush pngs into your drawable resources
* Copy BrushRevealView.kt into your project
* Add a `BrushRevealView` to the layout, covering the views which will load
* Call `reveal(view)` on the BrushRevealView when the covered views are finished loading, passing the root view of the layout you wish to reveal

#### For the splash screen to reveal transition
* Copy ic_bbc_sounds.xml and splash_screen.xml (both from drawable and drawable-v23) to your drawables
* Set `@drawable/splash_screen` as the value of `android:windowBackground` in your applications' theme

### Notes
#### Layout
The `BrushRevealView` was written in the expectation that it would fill the screen, but it does not enforce nor explicitly require it. The reveal animation should work when covering only subsections of the screen, though this has not been tested, and its attempts to match the position of the animated logo to the splash screen's logo position is likely to look odd if it does not at least cover the middle of the screen.

#### minSdk
The minSdk is 23 to allow us to use width and height attributes for the vector logo on the splash screen. Exchanging it for a png, or a vector path tuned for the right size, would let us forgo those attributes and allow lowering the minSdk to 21.

Going below 21 would require more work, but should also be feasible: the vector logos would have to be replaced with pngs or compat vectors and we would have to write our own Bezier curve implementation of TimeInterpolator to replace the PathInterpolator.

#### Clear the runway
Make sure there is no taxing processes going as the reveal animation runs, as Android will not give priority to keeping the animation smooth. E.g. the important parts the UI is loaded, so we start the reveal animation, but one part of the UI is still adding asynchronously fetched items to a RecyclerView, causing little spikes of layout processing.

### Questions?
**Contact Arild Jacobsen**

* phone: +47 958 88 163
* email: arild.jacobsen@agens.no
* slack: @arild.jacobsen
