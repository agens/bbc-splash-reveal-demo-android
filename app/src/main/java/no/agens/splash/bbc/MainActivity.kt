package no.agens.splash.bbc

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.animation.DecelerateInterpolator
import no.agens.splash.bbc.bbcsplashscreendemo.R

class MainActivity : AppCompatActivity() {
    private var hasLoaded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        if (!hasLoaded){
            fakeLoading()
        }
    }

    private fun fakeLoading() {
        Handler().postDelayed(this::onLoadingFinished, 2000L)
    }

    private fun onLoadingFinished() {
        hasLoaded = true
        val interfaceView = findViewById<View>(R.id.activity_main_ui_standin)
        findViewById<BrushRevealView>(R.id.activity_main_reveal_view)
            .reveal(interfaceView)
    }
}
