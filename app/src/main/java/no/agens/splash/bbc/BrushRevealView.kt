package no.agens.splash.bbc

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.PathInterpolator
import android.widget.FrameLayout
import android.widget.ImageView
import com.airbnb.lottie.LottieAnimationView
import com.airbnb.lottie.LottieDrawable
import no.agens.splash.bbc.bbcsplashscreendemo.R
import android.graphics.Bitmap
import android.os.Build
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.widget.SeekBar

class BrushRevealView : FrameLayout {
    private var viewWidth: Float = 0F
    private var viewHeight: Float = 0F
    private var endScale: PointF = PointF()
    private var revealedBitmap: Bitmap? = null

    private val brushMatrix = Matrix()
    private val brushMask by lazy {
        BitmapFactory.decodeResource(resources, R.drawable.brush)
    }

    private val brushPaint by lazy {
        Paint().apply {
            color = context.compatGetColor(R.color.bbcOrange)
        }
    }

    private val revealPaint = Paint().apply {
        xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
    }

    private val colorPaint by lazy {
        Paint().apply {
            color = context.compatGetColor(R.color.bbcOrange)
            xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OVER)
        }
    }

    private val animationView by lazy {
        LottieAnimationView(context).apply {
            setAnimation(R.raw.bbc_sounds_loading_no_pulse)
            repeatCount = LottieDrawable.INFINITE
            playAnimation()
        }
    }

    private val logoView by lazy {
        ImageView(context).apply {
            setImageResource(R.drawable.ic_bbc_logo)
        }
    }

    private val brushAnimator = ValueAnimator().apply {
        duration = 1000L
        // we're just using the animated fraction,
        // so the actual values don't matter
        setFloatValues(0F, 1F)
        interpolator = PathInterpolator(0.5F, 0.0F, 0.1F, 0.8F)
        addUpdateListener {
            transformBrush(it.animatedFraction)
            invalidate()
        }
        addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                visibility = GONE
                animationView.pauseAnimation()
                revealedBitmap = null
            }
        })
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int)
            : super(context, attrs, defStyleAttr, defStyleRes)

    init {
        setLayerType(View.LAYER_TYPE_HARDWARE, null)
        setWillNotDraw(false)
        // the actual logo is only about 70% of the width of the animation,
        // so we adjust up to match the splash screen vector
        val width = resources.getDimensionPixelSize(R.dimen.splash_sounds_logo_width) * 1.456
        addView(animationView, LayoutParams(
            width.toInt(),
            ViewGroup.LayoutParams.WRAP_CONTENT,
            Gravity.CENTER
        ).apply {
            // either the logo is not perfectly centered in the animation,
            // or there is some rounding issue, either way,
            // these slight adjustments seem to make the transition
            // from splash screen to loading screen
            // extremely hard to spot rather than just very hard
            bottomMargin = -(resources.displayMetrics.density * 1).toInt()
            rightMargin = -(resources.displayMetrics.density * 1).toInt()
        })
        addView(logoView, LayoutParams(
            resources.getDimensionPixelSize(R.dimen.reveal_bbc_logo_width),
            ViewGroup.LayoutParams.WRAP_CONTENT,
            Gravity.CENTER
        ).apply {
            topMargin = -(resources.getDimensionPixelSize(R.dimen.splash_bbc_logo_offset) / 2)
        })
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        viewWidth = measuredWidth.toFloat()
        viewHeight = measuredHeight.toFloat()

        endScale.x = (viewWidth / brushMask.width) * 3.5F
        endScale.y = (viewHeight / brushMask.height) * 3.5F

        brushMatrix.setScale(0F, 0F)
    }

    private val screenLocation = IntArray(2)
    private val realSize = Point()

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        (context as? Activity)?.windowManager?.defaultDisplay?.getRealSize(realSize)
        getLocationOnScreen(screenLocation)
        val topOffset = screenLocation[1]
        val bottomOffset = realSize.y - topOffset - viewHeight
        val offsetToMatchSplashCenter = bottomOffset / 2F - topOffset / 2F
        animationView.offsetTopAndBottom(offsetToMatchSplashCenter.toInt())
        logoView.offsetTopAndBottom(offsetToMatchSplashCenter.toInt())
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        revealedBitmap?.let { bm ->
            canvas.drawBitmap(brushMask, brushMatrix, brushPaint)
            canvas.drawBitmap(bm, 0F, 0F, revealPaint)
        }
        canvas.drawRect(0F, 0F, viewWidth, viewHeight, colorPaint)
    }

    fun reveal(view: View) {
        revealedBitmap = loadBitmapFromView(view)
        if (!brushAnimator.isRunning) {
            brushAnimator.start()
        }
    }

    private fun transformBrush(fraction: Float) {
        val brushWidthScale = endScale.x * fraction
        val brushHeightScale = endScale.y * fraction
        val brushWidth = brushMask.width * brushWidthScale
        val brushHeight = brushMask.height * brushHeightScale
        val yStart = viewHeight * 1.4F
        val yTarget = viewHeight / 2F - brushHeight / 2F
        val y = yStart + (yTarget - yStart) * fraction
        val x = (viewWidth / 2F) * fraction - brushWidth * 0.5F

        brushMatrix.reset()
        brushMatrix.postRotate(22F, brushMask.width / 2F, brushMask.height / 2F)
        brushMatrix.postScale(brushWidthScale, brushHeightScale)
        brushMatrix.postTranslate(x, y)

        val fadeOut = 1F - when {
            fraction < 0.5F -> 0F
            fraction > 0.6F -> 1F
            else -> (fraction - 0.5F) * 10F
        }
        animationView.alpha = fadeOut
        logoView.alpha = fadeOut
    }

    private fun loadBitmapFromView(view: View) = Bitmap
        .createBitmap(viewWidth.toInt(), viewHeight.toInt(), Bitmap.Config.RGB_565)
        .apply { view.draw(Canvas(this)) }
}

@ColorInt
private fun Context.compatGetColor(@ColorRes colorId: Int) =
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
        @Suppress("DEPRECATION")
        resources.getColor(colorId)
    } else {
        getColor(colorId)
    }
